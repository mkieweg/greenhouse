/*
	Service that sends dummy values to the greenhouse exporter.
*/

package main

import (
	"math/rand"
	"net"
	"time"

	pb "gitlab.com/mkieweg/greenhouse/api/go"
	"google.golang.org/protobuf/proto"

	log "github.com/sirupsen/logrus"
)

func main() {
	for {
		m := &pb.Measurement{
			DeviceID:      -1,
			MeasurementId: rand.Int31(),
			HumiditySoil: []float32{
				rand.Float32(),
				rand.Float32(),
				rand.Float32(),
				rand.Float32(),
				rand.Float32(),
				rand.Float32(),
				rand.Float32(),
				rand.Float32(),
				rand.Float32(),
				rand.Float32(),
			},
			HumidityAir: rand.Float32(),
			Temperature: rand.Float32() * float32(rand.Int31n(60)),
		}

		conn, err := net.Dial("tcp", "greenhouse.kieweg.dev:36363")
		if err != nil {
			log.Error(err)
		}

		buf, err := proto.Marshal(m)
		if err != nil {
			log.Error(err)
		}
		n, err := conn.Write(buf)
		if err != nil {
			log.Error(err)
		}
		log.WithField("bytes written", n).Info("sent bogus data")
		conn.Close()
		time.Sleep(time.Minute)
	}
}
