/*
	Bridge between sensor values and Prometheus
*/

package main

import (
	"os"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mkieweg/greenhouse"
)

func main() {
	level := os.Getenv("GREENHOUSE_LOGLEVEL")
	switch level {
	case "debug":
		log.SetLevel(log.DebugLevel)
		log.SetReportCaller(true)
	case "info":
		log.SetLevel(log.InfoLevel)
	default:
		log.SetLevel(log.ErrorLevel)
		log.SetFormatter(&log.JSONFormatter{})
	}
	log.Info(log.GetLevel())
	if err := greenhouse.Run(); err != nil {
		log.Fatal(err)
	}
}
