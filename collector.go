package greenhouse

import (
	"fmt"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	log "github.com/sirupsen/logrus"
	pb "gitlab.com/mkieweg/greenhouse/api/go"
)

var (
	humidityAir = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: "greenhouse",
		Subsystem: "esp8266",
		Name:      "humidity_air_ratio",
	})

	temperature = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: "greenhouse",
		Subsystem: "esp8266",
		Name:      "temperature_celsius",
	})

	humiditySoil = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "greenhouse",
		Subsystem: "esp8266",
		Name:      "humidity_soil_ratio",
	},
		[]string{
			"placement",
			"sensor_id",
		},
	)

	incomingMeasurement = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "greenhouse",
		Subsystem: "core",
		Name:      "measurements_total",
	})

	incomingMeasurementBytes = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "greenhouse",
		Subsystem: "core",
		Name:      "measurements_bytes",
	})
)

func processMeasurement(m *pb.Measurement) {
	log.Info(m)
	humidityAir.Set(float64(m.HumidityAir))
	temperature.Set(float64(m.Temperature))
	for i, val := range m.HumiditySoil {
		if val != 0.0 {
			sensor := fmt.Sprintf("sensor %v", i)
			humiditySoil.With(prometheus.Labels{"placement": sensor, "sensor_id": strconv.Itoa(int(m.DeviceID))}).Set(float64(val))
		}
	}
}
