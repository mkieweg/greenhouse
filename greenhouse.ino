#include <pb.h>
#include <pb_common.h>
#include <pb_encode.h>
#include <pb_decode.h>
#include <ESP8266WiFi.h>
#include <Wire.h>
#include <Adafruit_ADS1X15.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#include "api/esp8266/proto/greenhouse.pb.c"

/*
Define the following in config.h
  WIFI_PASSWORD
  WIFI_SSID
  GATEWAY_IP
  GATEWAY_PORT
  DEVICE_ID
  MEASUREMENT_ID
  BME280_ADDRESS
*/
#include "config.h"

const char* ssid     = WIFI_SSID;
const char* password = WIFI_PASSWORD;
const char* addr     = GATEWAY_IP;
const uint16_t port  = GATEWAY_PORT;
const int32_t deviceID = DEVICE_ID;
const int32_t measurementID = MEASUREMENT_ID;

WiFiClient client;
Adafruit_ADS1015 ads1015;
Adafruit_BME280 bme;

// setup WIFI and sensor
void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
  while (!Serial);

  Serial.println();
  Serial.print("Setting up WIFI for SSID ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    Serial.println("WIFI connection failed, reconnecting...");
    delay(500);
  }

  Serial.println("");
  Serial.print("WiFi connected, ");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting sensors...");
  // Start sensors here if necessary
  ads1015.begin();

  if (!bme.begin(BME280_ADDRESS)) {
    Serial.println("Could not find a valid BME280 sensor, check wiring or BME-280 address!");
    while (1);
  }
  bme.setSampling(Adafruit_BME280::MODE_FORCED,
                  Adafruit_BME280::SAMPLING_X1, // temperature
                  Adafruit_BME280::SAMPLING_NONE, // pressure
                  Adafruit_BME280::SAMPLING_X1, // humidity
                  Adafruit_BME280::FILTER_OFF);
}


void loop() {
  digitalWrite(LED_BUILTIN, LOW);
  Serial.print("connecting to ");
  Serial.println(addr);

  if (!client.connect(addr, port)) {
    Serial.println("connection failed");
    Serial.println("wait 5 sec to reconnect...");
    delay(5000);
    return;
  }

  int32_t adc0, adc1, adc2, adc3;

  adc0 = ads1015.readADC_SingleEnded(0);
  adc1 = ads1015.readADC_SingleEnded(1);
  adc2 = ads1015.readADC_SingleEnded(2);
  adc3 = ads1015.readADC_SingleEnded(3);

  float humidity, temperature, pressure;
  bme.takeForcedMeasurement(); // has no effect in normal mode
    humidity = bme.readHumidity();
    temperature = bme.readTemperature();
    pressure = bme.readPressure();
  
    
  greenhouse_Measurement measurement = greenhouse_Measurement_init_zero;
  measurement.deviceID = deviceID;
  measurement.measurementId = measurementID;
  measurement.humidity_soil[0] = adc0;
  measurement.humidity_soil[1] = adc1;
  measurement.humidity_soil[2] = adc2;
  measurement.humidity_soil[3] = adc3;
  measurement.humidity_air = humidity;
  measurement.temperature = temperature;
  
  send(measurement);
  digitalWrite(LED_BUILTIN, HIGH);
  
  ESP.deepSleep(60e6);
}

void send(greenhouse_Measurement e) {
  uint8_t buffer[128];
  pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));
  
  if (!pb_encode(&stream, greenhouse_Measurement_fields, &e)){
    Serial.println("failed to encode temp proto");
    Serial.println(PB_GET_ERROR(&stream));
    return;
  }
  client.write(buffer, stream.bytes_written);
}
