module gitlab.com/mkieweg/greenhouse

go 1.16

require (
	github.com/prometheus/client_golang v1.10.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.7.1
	google.golang.org/protobuf v1.26.0
)
