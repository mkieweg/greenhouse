package greenhouse

import (
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	log "github.com/sirupsen/logrus"
)

var stopChan chan os.Signal
var addr string
var listener net.Listener

func Run() error {
	var err error
	listener, err = net.Listen("tcp", addr)
	if err != nil {
		log.Fatal(err)
	}
	log.WithFields(log.Fields{
		"listen address": addr,
	}).Info("listening for incoming connections")

	go listen()
	for {
		select {
		case <-stopChan:
			return shutdown()
		case <-time.Tick(time.Minute):
			log.Debug("up and running")
		}
	}
}

func init() {
	stopChan = make(chan os.Signal)
	addr = os.Getenv("GREENHOUSE_ADDR")
	if addr == "" {
		addr = ":36363"
	}

	signal.Notify(stopChan, syscall.SIGTERM, os.Interrupt)
	startHttpServer()
}

func shutdown() error {
	log.Info("shutting down greenhouse bridge")
	listener.Close()
	return stopHttpServer()
}
