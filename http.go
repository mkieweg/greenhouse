package greenhouse

import (
	"fmt"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"

	"context"
	"time"
)

var httpServer *http.Server

func stopHttpServer() error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	log.Info("shutting down http server")
	return httpServer.Shutdown(ctx)
}

func registerHttpHandler() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
		}
	}()
	http.HandleFunc("/livez", healthCheck)
	http.HandleFunc("/readyz", readynessCheck)
	http.Handle("/metrics", promhttp.Handler())
}

func startHttpServer() {
	registerHttpHandler()
	httpServer = &http.Server{Addr: ":8080"}
	go func() {
		log.Info(httpServer.ListenAndServe())
	}()
}

func healthCheck(writer http.ResponseWriter, request *http.Request) {
	writer.WriteHeader(http.StatusOK)
}

func readynessCheck(writer http.ResponseWriter, request *http.Request) {
	writer.WriteHeader(http.StatusOK)
}
