package greenhouse

import (
	"net"
	"sync"

	log "github.com/sirupsen/logrus"
	pb "gitlab.com/mkieweg/greenhouse/api/go"
	"google.golang.org/protobuf/proto"
)

var ringbuffer []*pb.Measurement
var counter int
var lock sync.RWMutex

func init() {
	ringbuffer = make([]*pb.Measurement, 10)
}

func listen() {
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Error(err)
			break
		}
		log.WithFields(log.Fields{
			"remote address": conn.RemoteAddr(),
		}).Info("incoming connection")
		go handleConnection(conn)
	}
}

func handleConnection(conn net.Conn) {
	defer func() {
		log.Info("closing connection")
		if err := conn.Close(); err != nil {
			log.WithFields(log.Fields{
				"error": err,
			}).Error("error closing connection")
		}
	}()

	buf := make([]byte, 128)
	n, err := conn.Read(buf)
	if err != nil {
		log.Error(err)
		return
	}
	if n <= 0 {
		log.WithFields(log.Fields{
			"n": n,
		}).Error("no data received")
	}

	var m pb.Measurement

	if err := proto.Unmarshal(buf[:n], &m); err != nil {
		log.WithFields(log.Fields{
			"buf": buf,
		}).Error("failed to unmarshal")
	}
	lock.Lock()
	ringbuffer[counter] = &m
	counter++
	lock.Unlock()
	incomingMeasurement.Add(1)
	incomingMeasurementBytes.Add(float64(n))
	processMeasurement(&m)
}
