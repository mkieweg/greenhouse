// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.15.8
// source: greenhouse.proto

package greenhouse

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Measurement struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	DeviceID      int32     `protobuf:"varint,1,opt,name=deviceID,proto3" json:"deviceID,omitempty"`
	MeasurementId int32     `protobuf:"varint,2,opt,name=measurementId,proto3" json:"measurementId,omitempty"`
	HumiditySoil  []float32 `protobuf:"fixed32,3,rep,packed,name=humidity_soil,json=humiditySoil,proto3" json:"humidity_soil,omitempty"`
	HumidityAir   float32   `protobuf:"fixed32,4,opt,name=humidity_air,json=humidityAir,proto3" json:"humidity_air,omitempty"`
	Temperature   float32   `protobuf:"fixed32,5,opt,name=temperature,proto3" json:"temperature,omitempty"`
}

func (x *Measurement) Reset() {
	*x = Measurement{}
	if protoimpl.UnsafeEnabled {
		mi := &file_greenhouse_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Measurement) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Measurement) ProtoMessage() {}

func (x *Measurement) ProtoReflect() protoreflect.Message {
	mi := &file_greenhouse_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Measurement.ProtoReflect.Descriptor instead.
func (*Measurement) Descriptor() ([]byte, []int) {
	return file_greenhouse_proto_rawDescGZIP(), []int{0}
}

func (x *Measurement) GetDeviceID() int32 {
	if x != nil {
		return x.DeviceID
	}
	return 0
}

func (x *Measurement) GetMeasurementId() int32 {
	if x != nil {
		return x.MeasurementId
	}
	return 0
}

func (x *Measurement) GetHumiditySoil() []float32 {
	if x != nil {
		return x.HumiditySoil
	}
	return nil
}

func (x *Measurement) GetHumidityAir() float32 {
	if x != nil {
		return x.HumidityAir
	}
	return 0
}

func (x *Measurement) GetTemperature() float32 {
	if x != nil {
		return x.Temperature
	}
	return 0
}

var File_greenhouse_proto protoreflect.FileDescriptor

var file_greenhouse_proto_rawDesc = []byte{
	0x0a, 0x10, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x12, 0x0a, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x22, 0xb9,
	0x01, 0x0a, 0x0b, 0x4d, 0x65, 0x61, 0x73, 0x75, 0x72, 0x65, 0x6d, 0x65, 0x6e, 0x74, 0x12, 0x1a,
	0x0a, 0x08, 0x64, 0x65, 0x76, 0x69, 0x63, 0x65, 0x49, 0x44, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05,
	0x52, 0x08, 0x64, 0x65, 0x76, 0x69, 0x63, 0x65, 0x49, 0x44, 0x12, 0x24, 0x0a, 0x0d, 0x6d, 0x65,
	0x61, 0x73, 0x75, 0x72, 0x65, 0x6d, 0x65, 0x6e, 0x74, 0x49, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x05, 0x52, 0x0d, 0x6d, 0x65, 0x61, 0x73, 0x75, 0x72, 0x65, 0x6d, 0x65, 0x6e, 0x74, 0x49, 0x64,
	0x12, 0x23, 0x0a, 0x0d, 0x68, 0x75, 0x6d, 0x69, 0x64, 0x69, 0x74, 0x79, 0x5f, 0x73, 0x6f, 0x69,
	0x6c, 0x18, 0x03, 0x20, 0x03, 0x28, 0x02, 0x52, 0x0c, 0x68, 0x75, 0x6d, 0x69, 0x64, 0x69, 0x74,
	0x79, 0x53, 0x6f, 0x69, 0x6c, 0x12, 0x21, 0x0a, 0x0c, 0x68, 0x75, 0x6d, 0x69, 0x64, 0x69, 0x74,
	0x79, 0x5f, 0x61, 0x69, 0x72, 0x18, 0x04, 0x20, 0x01, 0x28, 0x02, 0x52, 0x0b, 0x68, 0x75, 0x6d,
	0x69, 0x64, 0x69, 0x74, 0x79, 0x41, 0x69, 0x72, 0x12, 0x20, 0x0a, 0x0b, 0x74, 0x65, 0x6d, 0x70,
	0x65, 0x72, 0x61, 0x74, 0x75, 0x72, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x02, 0x52, 0x0b, 0x74,
	0x65, 0x6d, 0x70, 0x65, 0x72, 0x61, 0x74, 0x75, 0x72, 0x65, 0x42, 0x31, 0x5a, 0x2f, 0x67, 0x69,
	0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x6d, 0x6b, 0x69, 0x65, 0x77, 0x65, 0x67,
	0x2f, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x2f,
	0x67, 0x6f, 0x2f, 0x67, 0x72, 0x65, 0x65, 0x6e, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x62, 0x06, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_greenhouse_proto_rawDescOnce sync.Once
	file_greenhouse_proto_rawDescData = file_greenhouse_proto_rawDesc
)

func file_greenhouse_proto_rawDescGZIP() []byte {
	file_greenhouse_proto_rawDescOnce.Do(func() {
		file_greenhouse_proto_rawDescData = protoimpl.X.CompressGZIP(file_greenhouse_proto_rawDescData)
	})
	return file_greenhouse_proto_rawDescData
}

var file_greenhouse_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_greenhouse_proto_goTypes = []interface{}{
	(*Measurement)(nil), // 0: greenhouse.Measurement
}
var file_greenhouse_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_greenhouse_proto_init() }
func file_greenhouse_proto_init() {
	if File_greenhouse_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_greenhouse_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Measurement); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_greenhouse_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_greenhouse_proto_goTypes,
		DependencyIndexes: file_greenhouse_proto_depIdxs,
		MessageInfos:      file_greenhouse_proto_msgTypes,
	}.Build()
	File_greenhouse_proto = out.File
	file_greenhouse_proto_rawDesc = nil
	file_greenhouse_proto_goTypes = nil
	file_greenhouse_proto_depIdxs = nil
}
